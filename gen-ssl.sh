#!/usr/bin/env bash

mkdir ./nginx/ssl

openssl genrsa -out devdockerCA.key 2048
openssl req -x509 -new -nodes -key ./nginx/ssldevdockerCA.key -days 10000 -out ./nginx/ssl/devdockerCA.crt
openssl genrsa -out ./nginx/ssl/registry.key 2048
openssl req -new -key ./nginx/ssl/registry.key -out ./nginx/ssl/dev-docker-registry.com.csr
openssl x509 -req \
        -in ./nginx/ssl/dev-docker-registry.com.csr \
        -CA ./nginx/ssl/devdockerCA.crt \
        -CAkey ./nginx/ssl/devdockerCA.key \
        -CAcreateserial \
        -out ./nginx/ssl/registry.crt \
        -days 10000
